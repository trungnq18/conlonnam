﻿using System;
using System.Collections.Generic;
using System.Linq;
using LikeZ.UserControls;
using Xamarin.Forms;

namespace LikeZ.Models
{
	public class SwitcherPageViewModel : BaseViewModel
	{
		public SwitcherPageViewModel()
		{
			Pages = new List<HomeViewModel>() {
                new HomeViewModel { Title = "1", Background = Color.White, ImageSource = "ip6banner.jpg" },
                new HomeViewModel { Title = "2", Background = Color.Red, ImageSource = "banner1.jpg" },
                new HomeViewModel { Title = "3", Background = Color.Blue, ImageSource = "banner2.jpeg" },
                new HomeViewModel { Title = "4", Background = Color.Yellow, ImageSource = "banner3.jpg" },
			};

			CurrentPage = Pages.First();
		}

		IEnumerable<HomeViewModel> _pages;
		public IEnumerable<HomeViewModel> Pages {
			get {
				return _pages;
			}
			set {
				SetObservableProperty (ref _pages, value);
				CurrentPage = Pages.FirstOrDefault ();
			}
		}

		HomeViewModel _currentPage;
		public HomeViewModel CurrentPage {
			get {
				return _currentPage;
			}
			set {
				SetObservableProperty (ref _currentPage, value);
			}
		}
	}

	public class HomeViewModel : BaseViewModel, ITabProvider
	{

		public string Title { get; set; }
		public Color Background { get; set; }
		public string ImageSource { get; set; }
	}
}

