﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace LikeZ.Views
{
    public class MapPage : ContentPage
    {
        public MapPage()
        {
            var map = new Map
            {
                IsShowingUser = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(map);
            Content = stack;
        }
    }
}

