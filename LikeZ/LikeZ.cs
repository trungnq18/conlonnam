﻿using System;
using LikeZ.Views;
using Xamarin.Forms;

namespace LikeZ
{
    public class App : Application
    {
        public App()
        {
            var tabbPage = new TabbedPage();
            var homepage = new NavigationPage(new Dashboard());
            homepage.Icon = "ic_home";
            homepage.Title = "Nổi bật";

            var searchPage = new NavigationPage(new FindStore());
            searchPage.Icon = "ic_search";
            searchPage.Title = "Cửa hàng";
            tabbPage.Children.Add(homepage);
            tabbPage.Children.Add(searchPage);
            MainPage = tabbPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
