﻿using System;
using Xamarin.Forms;

namespace LikeZ.UserControls
{
    public class HomeView : ContentView
    {
        public HomeView()
        {
            BackgroundColor = Color.White;

            var image = new Image
            {
                Aspect= Aspect.AspectFill,
                HorizontalOptions= LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
                              
            };
            image.SetBinding(Image.SourceProperty, "ImageSource");
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
                    image
                }
            };
        }
    }
}
