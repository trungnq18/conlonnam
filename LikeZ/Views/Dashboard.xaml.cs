﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LikeZ.Views
{
    public partial class Dashboard : ContentPage
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new DetailPages());
        }
    }
}
