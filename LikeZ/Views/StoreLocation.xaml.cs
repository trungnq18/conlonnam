﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace LikeZ.Views
{
    public partial class StoreLocation : ContentPage
    {
        // The curent pin
         Pin _pinLikeZ = new Pin()
        {
            Type = PinType.Place,
            Label = "LikeZone Tôn Thất Tùng",
            Address = "Số 10 Ngõ 10 Tôn Thất Tùng",
            Position = new Position(21.004209,105.8271733),
            Icon = BitmapDescriptorFactory.FromBundle("ic_pin")
        };
        public StoreLocation()
        {
            InitializeComponent();
            map.Pins.Add(_pinLikeZ);
            map.MoveToRegion(MapSpan.FromCenterAndRadius(_pinLikeZ.Position, Distance.FromMeters(1000)), true);
			map.SelectedPin = _pinLikeZ;
        }
    }
}
