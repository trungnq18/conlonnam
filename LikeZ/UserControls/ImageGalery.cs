﻿using System;
using LikeZ.Models;
using Xamarin.Forms;

namespace LikeZ.UserControls
{
    public class ImageGalery : ContentView
    {
        RelativeLayout relativeLayout;

        CarouselLayout.IndicatorStyleEnum _indicatorStyle;

        SwitcherPageViewModel viewModel;
        public ImageGalery()
        {
            _indicatorStyle = CarouselLayout.IndicatorStyleEnum.Dots;

            viewModel = new SwitcherPageViewModel();
            BindingContext = viewModel;

            relativeLayout = new RelativeLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            var pagesCarousel = CreatePagesCarousel();
            var dots = CreatePagerIndicatorContainer();

            relativeLayout.Children.Add(pagesCarousel,
                        Constraint.RelativeToParent((parent) => { return parent.X; }),
                        Constraint.RelativeToParent((parent) => { return parent.Y; }),
                        Constraint.RelativeToParent((parent) => { return parent.Width; }),
                        Constraint.RelativeToParent((parent) => { return parent.Height; })
                    );

            relativeLayout.Children.Add(dots,
                Constraint.Constant(0),
                Constraint.RelativeToView(pagesCarousel,
                    (parent, sibling) => { return sibling.Height - 18; }),
                Constraint.RelativeToParent(parent => parent.Width),
                Constraint.Constant(18)
            );
            Content = relativeLayout;

        }
		CarouselLayout CreatePagesCarousel()
		{
			var carousel = new CarouselLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				IndicatorStyle = _indicatorStyle,
				ItemTemplate = new DataTemplate(typeof(HomeView))
			};
			carousel.SetBinding(CarouselLayout.ItemsSourceProperty, "Pages");
			carousel.SetBinding(CarouselLayout.SelectedItemProperty, "CurrentPage", BindingMode.TwoWay);

			return carousel;
		}
		View CreatePagerIndicatorContainer()
		{
			return new StackLayout
			{
				Children = { CreatePagerIndicators() }
			};
		}
		View CreatePagerIndicators()
		{
			var pagerIndicator = new PagerIndicatorDots() { DotSize = 5, DotColor = Color.Black };
			pagerIndicator.SetBinding(PagerIndicatorDots.ItemsSourceProperty, "Pages");
			pagerIndicator.SetBinding(PagerIndicatorDots.SelectedItemProperty, "CurrentPage");
			return pagerIndicator;
		}
    }
}

