﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LikeZ.Views
{
    public partial class FindStore : ContentPage
    {
        public FindStore()
        {
            InitializeComponent();
            store1.GestureRecognizers.Add(new TapGestureRecognizer{
                Command = new Command(async ()=>{
                    await Navigation.PushAsync(new StoreLocation());
                })
            });
        }
    }
}
