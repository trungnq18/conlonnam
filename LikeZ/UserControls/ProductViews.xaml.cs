﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LikeZ.UserControls
{
    public partial class ProductViews : ContentView
    {
        public event EventHandler Clicked;

        public ProductViews()
        {
            InitializeComponent();

            GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    Clicked?.Invoke(this, new EventArgs());
                })
            });
        }
    }
}
